
import { Operation } from "../entity/Opentity";
import { connection } from "./connection";




export class OpRepository {
    
    static async findById(id){
        const [rows] = await connection.query('SELECT * FROM operation  WHERE id=?',[id]);
        const row = rows[0];
        let instence = new Operation(row['id'],row['titre'],row['montant'], row['date'], row['categorie']);
        return instence;
    } 
    
    
    
    static async findAll(){
        const [rows] = await connection.query('SELECT * FROM operation');
        const operations = []; 
        for (const row of rows) {
            let instence = new Operation(row['id'], row['titre'], row['montant'], row['date'] ,row['categorie']);
            operations.push(instence);
        }
        return operations;
    }

    static async Add(operation){
        const [rows]=await connection.query('INSERT INTO operation (titre, montant , date, categorie ) VALUES (? , ?, ? ,?)',[operation.titre, operation.montant, operation.date, operation.categorie]);
        operation.id = rows.insertId;
    }

    static async Delete(id){
        await connection.query('DELETE FROM operation WHERE `id` = ? ',[id]);

    }

    static async Update(operation){
        await connection.query('UPDATE operation SET titre=?, montant=?, date=?, categorie=? WHERE id=?',[operation.titre, operation.montant, operation.date, operation.categorie, operation.id]);
    }


    static async findByCat(categorie){
       const [rows] = await connection.query('SELECT * FROM operation  WHERE categorie=?',[categorie]);
        const operations = []; 
        for (const row of rows) {
            let instence = new Operation(row['id'], row['titre'], row['montant'], row['date'] ,row['categorie']);
            operations.push(instence);
        }
        return operations;
    }

    static async findByMonth(month){
        const [rows] = await connection.query('SELECT * FROM operation WHERE MONTH(date)=?',[month]);
        const operations = [];
        for (const row of rows){
            let instence = new Operation(row['id'], row['titre'], row['montant'], row['date'], row['categorie']);
            operations.push(instence);
        }
        return operations;
    }

    static async search(term){
        const [rows] = await connection .query('SELECT * FROM operation WHERE CONCAT(titre, montant, date, categorie)LIKE ?',['%'+term+'%']);
        return rows.map(row => new Operation(row['id'], row['titre'], row['montant'],row['date'] , row['categorie']));

    }
}