export class Operation{
    id;
    titre;
    montant;
    date;
    categorie;
    
    constructor(id = null, titre, montant, date, categorie){
    this.id = id;    
    this.titre = titre;
    this.montant = montant;
    this.date = date;
    this.categorie = categorie;
    }

}