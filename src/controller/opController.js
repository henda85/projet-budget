import { Router } from "express";
import { OpRepository } from "../repository/opRepository";
export const controller = Router();


controller.get('/:id', async(req, res)=>{
    try {
        let data = await OpRepository.findById(req.params.id);
        res.json(data);
        res.end;
    } catch (error) {
        console.log(error);
        res.status(500).end(); 
    }
})
controller.get('/',async(req,resp)=>{
    try {
        let operations;
        if(req.query.search){
            operations = await OpRepository.search(req.query.search);
        }else{
            operations = await OpRepository.findAll();
        }
        resp.json(operations);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

controller.post('/',async(req,res)=>{
    try {
        await OpRepository.Add(req.body);
        res.status(201).json(req.body); 
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

controller.delete('/:id',async(req,res)=>{
    try {
        await OpRepository.Delete(req.params.id);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

controller.put('/', async(req,res)=>{
    try {
        await OpRepository.Update(req.body);
    res.end();
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

controller.get('/categorie/:categorie',async(req,res)=>{
    try {
        let data = await OpRepository.findByCat(req.params.categorie);
        res.json(data);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

controller.get('/month/:month',async(req,res)=>{
    try {
        let data = await OpRepository.findByMonth(req.params.month);
        res.json(data);
        res.end();
        
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})