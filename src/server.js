import express from "express";
import { controller } from "./controller/opController.js";
import cors from 'cors';

export const server = express();
server.use(cors());

server.use(express.json());

server.get('/',async(req,res)=>{res.json('bienvenue')});
server.use('/api/operation', controller);
