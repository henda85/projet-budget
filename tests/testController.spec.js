import request from "supertest";
import {server} from "../src/server";


    describe('budget test',()=>{
        it('should return a operation table',async()=>{
            let response = await request(server)
                .get('/api/operation/')
                .expect(200);
            //la réponse doit contenir un tableau d'operation
            expect(response.body).toContainEqual({
                id: expect.any(Number),
                titre: expect.any(String),
                montant: expect.any(Number),
                date: expect.any(String),
                categorie: expect.any(String)
            });
        });
    });

